File Base64 formatter is a simple module that provides a way to encode files
as Base64. It works with file fields only. One example use-case might be where
you want to access a file attachment from a different domain with javascript.

Enable the module in the usual way. Use this file field formatter in the manage
display area of content type configuration screens, field formatters in Views,
etc.
